------------------------------------------------------------------   
# 推荐使用最新版的客户端，以保证正常使用！！！   
------------------------------------------------------------------   
# 客户端下载地址  
------------------------------------------------------------------   
## For SSR  
---------------------SSR------------------------------------------   
### For Windows   
#### https://d12.tlanyan.me:20186/ssr/windows/ShadowsocksR-win-4.9.2-tlanyan.zip    
### For Androd   
#### https://d12.tlanyan.me:20186/ssr/android/shadowsocksr-android-3.5.4.apk   
### For Mac  
#### https://d15.tlanyan.me:20625/ssr/macos/ShadowsocksX-NG-R8.dmg   
### For IOS  
#### https://tlanyan.me/get-proxy-clients/   
### For All   
#### https://tlanyan.me/shadowsockr-shadowsocksr-shadowsocksrr-clients/  
------------------V2Ray-------------------------------------------   
## For V2Ray   
### For All   
#### https://tlanyan.me/v2ray-clients-download/    
### For Windows  
#### https://github.com/2dust/v2rayN/releases/   
### For Android   
#### https://github.com/2dust/v2rayNG/releases/   
### For Mac   
#### https://github.com/yanue/V2rayU/releases/   
------------------------------------------------------------------   
# git    
## git 设置代理    
### git config --global http.proxy http://127.0.0.1:1080/   
### git config --global https.proxy http://127.0.0.1:1080/   
需要修改IP和端口号，改成正在运行SSR或者V2RAY的电脑的IP，改成SSR或者V2RAY的端口    
## git 查看代理   
### git config --global --get http.proxy   
### git config --global --get https.proxy   
## git 取消代理   
### git config --global --unset http.proxy   
### git config --global --unset https.proxy   
# Linux   
## 设置代理   
### export http_proxy=http://127.0.0.1:1080   
### export https_proxy=http://127.0.0.1:1080   
需要修改IP和端口号，改成正在运行SSR或者V2RAY的电脑的IP，改成SSR或者V2RAY的端口   
## 查看代理   
#### echo ${http_proxy}   
### echo ${https_proxy}   
## 取消代理   
### unset http_proxy https_proxy   
# google driver   
## 推荐使用 googleWgetDownload.sh 进行下载   
------------------------------------------------------------------
# MyVPS  
------------------------------------------------------------------    
## 查看是否 IP 被封   
### https://www.vps234.com/ipchecker/    
## 查看是否端口被封   
### http://tool.chinaz.com/port   
------------------------------------------------------------------    
## 客户端
请使用最新版本或者推荐版本的客户端，统一下载地址如下：  
### https://tlanyan.me/shadowsockr-shadowsocksr-shadowsocksrr-clients/
### https://tlanyan.me/v2ray-clients-download/   
#### SSR速度比v2ray快，但是v2ray不易被墙，非特殊时期即可使用SSR   
------------------------------------------------------------------    
## 教程   
### For SSR   
#### https://hijk.art/shadowsocksr-ssr-windows-client-config-tutorial/   
### For v2ray   
#### https://hijk.art/v2rayn-config-tutorial/    
------------------------------------------------------------------    
## IPv6
IPv6地址的链接仅支持设备能成功获取到IPv6地址的网络环境  
例如:校园网   
建议使用SSR的均衡负载模式，不易断线。   
------------------------------------------------------------------       
## PAC   
推荐使用PAC模式  
更新PAC为绕过大陆常见域名列表  
如果不好用 ,就更新PAC为GFWList  
代理规则->绕过局域网和大陆  
### 谷歌问题    
youtube能打开但是google打不开  
PAC 更新失败 Failed To Update PAC File  
尝试使管理员权限的命令提示符 CMD 或者 PowerShell  
运行 `ipconfig /flushdns` 来更新电脑的DNS缓存  
### PAC解释   
PAC(Proxy Auto Config) 是一个 Script；  
经由编写这个 Script，我们可以让系统判断在怎么样的情形下，要利用哪一台 Proxy 来进行联机。  
白话就是,这是一个列表,属于这个列表的走代理,不属于的,不走代理,直连  
列表只能说是比较全的,但是也有错误,有可能把国内的地址走了代理,没有代理某些国外地址  
如果你需要访问的地址是国外而且PAC不包含,你可以短时间使用全局模式,之后切换回来  
全局模式访问国内网址,国内网址的服务器会检测到流量来自国外,增加被封的可能性,  
所以不要长时间全局模式,更不要访问一些敏感信息的网站,无论国内国外.  
------------------------------------------------------------------    
## 订阅   
如果更新订阅失败,请换成[直连模式],然后载更新订阅,
意思是,更新订阅的时候,关闭代理,gitee是国内的,不要通过代理去访问    
如果更新订阅失败,请强烈建议更换SSR_WIN版本为`4.9.2修复版`的版本  
这个版本有[更新订阅(`不`通过代理)]的选项,不走代理更新,不会导致无法访问gitee  
------------------------------------------------------------------    
## 教程   
### 提示 不同系统 不同版本 描述文字和按钮位置不同 但思路一致  
*******************************************************************
# For V2Ray  
*******************************************************************  
V2ray是一种新的架构,是专门防被封的,    
推荐下载V2Ray ,添加订阅  
下载和教程 https://tlanyan.me/v2ray-clients-download/  
订阅地址是  (注意:vmess全小写,而且和名字之间有个下划线)  
{   
##  `https://gitee.com/leeherry/MyVPS/raw/master/你的姓名全拼首字母大写.vmess`
}  
注意: V2Ray客户端有更新订阅按钮,不能上网点一下  
注意: V2Ray客户端有检查更新按钮,不时的手动更新一下'v2rayN','v2rayCore'和'PAC' !!!  
# For SSR  
***********************************************************************   
# For WIN PC:  
强烈推荐`客户端`文件夹里面的SSR版本,  
下载地址 https://tlanyan.me/shadowsockr-shadowsocksr-shadowsocksrr-clients/   
`  4.9.2修复版  4.9.2修复版  4.9.2修复版  `    
教程 https://www.hijk.pw/shadowsocksr-ssr-windows-client-config-tutorial/  
在桌面右下角状态栏里面,找到小飞机上,右击->服务器订阅->服务器订阅设置->ADD->输入:   
{  (注意:ssr全小写,而且和名字之间有个下划线)      
##  `https://gitee.com/leeherry/MyVPS/raw/master/你的姓名全拼首字母大写.ssr`
}   
打钩 '自动更新'  ->  点击 "确定"  
在小飞机上,右击->服务器订阅 -> 更新服务器订阅  
等待提示更新成功 或者 服务器列表刷新(有的软件不弹窗提示)  

注意:添加网址的那一步,只需要一次,只需要一次,只需要一次   
理论上,即使将来FIX ISP BLOCK 导致 IP Change   
也不需要进行进行任何操作,等待小飞机软件自动去更新订阅就行   

如果,小飞机没有自动更新,则需要手动更新   
手动更新->右击小飞机图标->服务器订阅->服务器更新订阅   
一次不成功的话,多试几次.   

# For Android ShadowsocksR Apk   
## 教程 https://www.hijk.pw/shadowsocksr-ssr-ssrr-android-config-tutorial/  
点击 右上角 加号图标 -> 添加/更新订阅  
添加订阅地址 -> 输入 你的订阅地址[见上]  
点击 确定 -> 打开自动订阅的滑块 -> 点击 确定并更新  
手动更新方法: 右上角加号图标 -> 添加/更新订阅 -> 确定并更新  

# For Shadowrocket IOS  
## 教程 https://tlanyan.me/get-proxy-clients/  
右上角加号 -> 类型 -> 选择 ' Subscribe ' -> URL 填入 你的订阅地址[见上] -> 右上角 完成  
回到软件首页 -> 等待刷新 -> 找到 对应的地址 -> 点击  

# For Mac  
## 教程 https://www.hijk.pw/ssr-shadowsocksx-ng-config-tutorial/   
同理 找到添加订阅的按钮 -> 输入订阅地址 -> 更新订阅  



