#!/bin/bash
echo "本脚本用于下载谷歌云盘的单个文件，不支持下载文件夹."
echo "如果要下载文件夹,请使用谷歌云盘的打包下载,下载文件夹的压缩包"
echo "请输入要下载的文件的下载地址，或者共享地址，或者文件ID"
echo "例如：文件ID：1xbYbZ7rpyjftI_KCk6YuL-XrfQDz7Yd4,特征是id=后面的字符串,不包括其他&"
echo "例如：右键获取文件共享地址：https://drive.google.com/file/d/1fFN5J2He6eTqMPRl_M9gFtFfpUmhtQc9/view?usp=sharing，特征是存在\"/view\""
echo "例如：文件链接：https://drive.google.com/u/0/uc?export=download&confirm=Q8Ua&id=1IJSp_t5SRlQarFClrRolQzSJ4K5xZIqm，特征是存在id=···"
echo "例如：浏览器任务列表右键复制下载地址：https://doc-0s-64-docs.googleusercontent.com/docs/securesc/e6kdl8d0l26o6fgc1eemlnsp888ac9up/icts0ro47764fvd0tfemvau4q6171994/1599307575000/13520325269685372363/11846996457835063367/1Nb4abvIkkp_ydPFA9sNPT1WakoVKA8Fa?e=download&authuser=0&nonce=ive7pahm421pm&user=11846996457835063367&hash=qp7sub0sbvccfvapdnn158en59re9foj，特征是一长串，在浏览器下载的时候，右键，复制下载地址，就可以得到这个地址"
read link
id=$(echo "${link}" | sed -r 's/\/view//g' | sed -r 's/id=/\//g' | rev | cut -d/ -f1 | rev | cut -d\? -f1 | cut -d\& -f1 )
URL="https://docs.google.com/uc?export=download&id=${id}"
echo '你要下载的谷歌云盘的文件的ID是：' ${id}

echo "请输入保存为的文件的名字，包括后缀："
read name
echo "将把文件储存为：" ${name}

proxy=‘’
echo "是否设置自定义代理服务器? [Y/n]:"
read yn
case $yn in 
 [yY][eE][sS]|[yY])
 echo "开始设置自定义代理服务器."
 echo "请输入代理服务器的IP地址，即正在运行ShadowsocksR或者V2ray的WindowsPC机器的IPv4地址："
 read ip
 echo "你输入的IP是："${ip}

 echo "如果你是ShadowsocksR客户端，请在桌面右下角图标上，右击，选项设置，允许来自局域网的连接，打钩，自定义并记住本地端口，用户名和密码！必须！留空，确定。"
 echo "如果你是V2ray客户端，请双击打开V2ray，参数设置，Core:基础设置，本地监听端口，自定义并记住本地端口号，v2ray设置，允许来自局域网的连接，打钩，确定。"
 echo "请输入代理服务器的本地端口，例如1080："
 read port
 echo "你输入的端口号是："${port}

 echo "你的代理服务器的设置是->"
 echo "export http_proxy=http://"${ip}":"${port}"/"
 echo "export https_proxy=http://"${ip}":"${port}"/"

 export http_proxy="http://"${ip}":"${port}"/"
 export https_proxy="http://"${ip}":"${port}"/"

# proxy=" -e export  http_proxy=\"http://"${ip}":"${port}"/\" "
 ;;
 [nN][oO]|[nN])
 echo "不设置自定义服务器."
 ;;
 *)
 echo "输入类型错误，不设置自定义服务器."
 exit 1
esac

echo "开始下载..."
wget  --load-cookies cookies.txt "https://docs.google.com/uc?export=download&confirm=$(wget --quiet --save-cookies cookies.txt --keep-session-cookies --no-check-certificate $URL -O- | sed -rn 's/.*confirm=([0-9A-Za-z_]+).*/\1\n/p')&id=${id}" -O ${name} && rm -rf cookies.txt
echo "下载完成。"

echo "是否清理设置的自定义代理服务器? [Y/n]:"
read yn
case $yn in 
 [yY][eE][sS]|[yY])
 echo "清理自定义代理服务器."

 unset http_proxy
 unset https_proxy

 echo "清理自定义代理服务器完成."

 ;;
 [nN][oO]|[nN])
 echo "不清理自定义代理服务器，退出shell后自动失效."
 ;;
 *)
 echo "输入类型错误，不清理自定义代理服务器，退出shell后自动失效."
 exit 1
esac

echo "程序完成，脚本退出."



# By: LiHuaiRui 
# 2020年9月7日15:22:05
