#! /bin/bash

ips=`netstat -ntu | grep tcp | awk '{print $5}' | cut -d: -f1 | sort | uniq`

for ip in $ips;
do
#	curl http://freeapi.ipip.net/$ip
	IP_address=`wget -qO- -t1 -T2 http://freeapi.ipip.net/$ip|sed 's/\"//g;s/,//g;s/\[//g;s/\]//g'`
	echo $ip,$IP_address
done

