#! /bin/bash

change=0

cat /var/log/secure|awk '/Failed/{print $(NF-3)}'|sort|uniq -c|awk '{print $2"="$1;}' > /usr/local/bin/black.list
for i in `cat  /usr/local/bin/black.list`
do
  IP=`echo $i |awk -F= '{print $1}'`
  NUM=`echo $i|awk -F= '{print $2}'`
  if [ ${#NUM} -gt 1 ]; then
    grep $IP /etc/hosts.deny > /dev/null
    if [ $? -gt 0 ];then
      change=1
      echo "sshd:$IP:deny" >> /etc/hosts.deny
    fi
  fi
done

if [ ${change} -ne "0" ];then
  ls -la /etc/hosts.deny
  echo "changed"
  rm -rf /root/MyVPS/hosts.deny
  for ip in $(cat /etc/hosts.deny | cut -d: -f2 | sort -t'.' -k1,1n -k2,2n -k3,3n -k4,4n | uniq)
  do
    echo "sshd:${ip}:deny" >> /root/MyVPS/hosts.deny
  done
  cp /root/MyVPS/hosts.deny /etc/hosts.deny
fi

