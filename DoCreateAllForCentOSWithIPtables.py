#-*- coding:utf-8 -*-
############################################################################################################################
# import ###################################################################################################################
############################################################################################################################
import json
import socket
import base64
import os
import time
import sys
############################################################################################################################
def get_ipv4():
    try:
        s = socket.socket(socket.AF_INET , socket.SOCK_DGRAM)
        s.connect(('8.8.8.8', 80))
        ipv4 = s.getsockname()[0]
    finally:
        s.close()
    return ipv4
############################################################################################################################
def get_ipv6():
    try:
        s = socket.socket(socket.AF_INET6, socket.SOCK_DGRAM)
        s.connect(('2001:4860:4860::8888', 80))
        ipv6 = s.getsockname()[0]
    finally:
	    s.close()
    return ipv6
############################################################################################################################
def DoBase64(str):
    return base64.urlsafe_b64encode(str.encode("utf-8")).replace('=','')
############################################################################################################################
def UnBase64(str):
    return base64.b64decode(str+ b'=' * 3).decode("utf-8") #加上三个等号
############################################################################################################################
# main begin ###############################################################################################################
############################################################################################################################
mypath = os.path.dirname(os.path.realpath(__file__))
print mypath
os.chdir(mypath)
os.system("cp /etc/hosts.deny" + " " + mypath)
os.system("cp /etc/crontab" + " " + mypath)
############################################################################################################################
# get vps ip ###############################################################################################################
############################################################################################################################
ipv4 = get_ipv4()
ipv6 = get_ipv6()
print '"VPS":{"IPv4":"',ipv4,'"; "IPv6":"',ipv6,'";}'
old_ip = str( '127.0.0.1' )
if os.path.exists(os.path.join(mypath, 'ipv4')):
    file_r = open(os.path.join(mypath, 'ipv4'), 'r')
    old_ip = str( file_r.readline() ).replace('\n','')
    file_r.close()
    print old_ip
    if old_ip != ipv4:
        os.system(" sed -i \"s/SERVER_PUB_ADDR = '`cat \"/usr/local/shadowsocksr/userapiconfig.py\"|grep \"SERVER_PUB_ADDR = \"|awk -F \"[']\" '{print $2}'`'/SERVER_PUB_ADDR = '" + ipv4 + "'/\" \"/usr/local/shadowsocksr/userapiconfig.py\" ")
        print 'ip changed! '
    else:
	print 'same ip ... '
else:
    os.system(" sed -i \"s/SERVER_PUB_ADDR = '`cat \"/usr/local/shadowsocksr/userapiconfig.py\"|grep \"SERVER_PUB_ADDR = \"|awk -F \"[']\" '{print $2}'`'/SERVER_PUB_ADDR = '" + ipv4 + "'/\" \"/usr/local/shadowsocksr/userapiconfig.py\" ")
    print 'ip changed! '
#exit()
############################################################################################################################
file_r = open(os.path.join(mypath, 'ipv4'), 'w+')
file_r.write(ipv4)
file_r.write('\n')
file_r.close()
file_r = open(os.path.join(mypath, 'ipv6'), 'w+')
file_r.write(ipv6)
file_r.write('\n')
file_r.close()
############################################################################################################################
# reset iptables ###########################################################################################################
############################################################################################################################
os.system("systemctl enable iptables")
os.system("systemctl enable ip6tables")
os.system("systemctl start iptables")
os.system("systemctl start ip6tables")
os.system("systemctl status iptables")
os.system("systemctl status ip6tables")

os.system("iptables -P INPUT ACCEPT")
os.system("iptables -F")
os.system("iptables -A INPUT -p tcp --dport 22 -j ACCEPT")
#os.system("iptables -A INPUT -p tcp --dport 21 -j ACCEPT")
#os.system("iptables -A INPUT -p tcp --dport 80 -j ACCEPT")
#os.system("iptables -A INPUT -p tcp --dport 443 -j ACCEPT")
os.system("iptables -A INPUT -p tcp --dport 5001 -j ACCEPT")
os.system("iptables -A INPUT -p tcp --dport 48888 -j ACCEPT")
os.system("iptables -A INPUT -p icmp --icmp-type echo-request -j ACCEPT")
os.system("iptables -A OUTPUT -p icmp --icmp-type echo-reply -j ACCEPT")
os.system("service iptables save")
os.system("systemctl restart iptables")

os.system("ip6tables -P INPUT ACCEPT")
os.system("ip6tables -F")
os.system("ip6tables -A INPUT -p tcp --dport 22 -j ACCEPT")
#os.system("ip6tables -A INPUT -p tcp --dport 21 -j ACCEPT")
#os.system("ip6tables -A INPUT -p tcp --dport 80 -j ACCEPT")
#os.system("ip6tables -A INPUT -p tcp --dport 443 -j ACCEPT")
os.system("ip6tables -A INPUT -p tcp --dport 5001 -j ACCEPT")
os.system("ip6tables -A INPUT -p tcp --dport 48888 -j ACCEPT")
os.system("iptables -A INPUT -p icmp --icmp-type echo-request -j ACCEPT")
os.system("iptables -A OUTPUT -p icmp --icmp-type echo-reply -j ACCEPT")
os.system("service ip6tables save")
os.system("systemctl restart ip6tables")

############################################################################################################################
#  reset firewall ##########################################################################################################
############################################################################################################################
os.system("systemctl enable firewalld")
os.system("systemctl start firewalld")
os.system("systemctl status firewalld")
output = os.popen('firewall-cmd --list-ports')
ports_list = str(output.read()).replace('\n','')
ports_list = filter(None,ports_list.split(" "))
print ports_list
for port in ports_list:
    print '\r',"firewall-cmd --zone=public --remove-port=" , str(port) , " --permanent"," ==> ",
    sys.stdout.flush()
    os.system("firewall-cmd --zone=public --remove-port=" + str(port) + " --permanent")
    #end loop
# SSH
print '\r',"firewall-cmd --zone=public --add-port=22/tcp --permanent"," ==> ",
sys.stdout.flush()
os.system("firewall-cmd --zone=public --add-port=22/tcp --permanent")
# 21
#print '\r',"firewall-cmd --zone=public --add-port=21/tcp --permanent"," ==> ",
#sys.stdout.flush()
#os.system("firewall-cmd --zone=public --add-port=21/tcp --permanent")
# 80
#print '\r',"firewall-cmd --zone=public --add-port=80/tcp --permanent"," ==> ",
#sys.stdout.flush()
#os.system("firewall-cmd --zone=public --add-port=80/tcp --permanent")
# 443
#print '\r',"firewall-cmd --zone=public --add-port=443/tcp --permanent"," ==> ",
#sys.stdout.flush()
#os.system("firewall-cmd --zone=public --add-port=443/tcp --permanent")
# BaoTa
print '\r',"firewall-cmd --zone=public --add-port=48888/tcp --permanent"," ==> ",
sys.stdout.flush()
os.system("firewall-cmd --zone=public --add-port=48888/tcp --permanent")
# iperf
print '\r',"firewall-cmd --zone=public --add-port=5001/tcp --permanent"," ==> ",
sys.stdout.flush()
os.system("firewall-cmd --zone=public --add-port=5001/tcp --permanent")
############################################################################################################################
# delete files #############################################################################################################
############################################################################################################################
os.system("rm -rf *.txt")
os.system("rm -rf *.ssr")
os.system("rm -rf *.vmess")
############################################################################################################################
#### SSR ###################################################################################################################
############################################################################################################################
ssr_url_all = ''

os.system("cp /usr/local/shadowsocksr/mudb.json" + " " + mypath)
filename = "/usr/local/shadowsocksr/mudb.json"

if os.path.exists(filename):
    file_obj = open(filename,"r")
    ssrs = json.load(file_obj)
    #print ssrs
    for ssr in ssrs:
        #print ssr
        user = str( ssr['user'] )
        method = str( ssr['method'] )
        obfs = str( ssr['obfs'] )
        port = str( ssr['port'] )
        protocol = str( ssr['protocol'] )
        passwd = str( DoBase64(ssr['passwd']) )
        if 'obfs_param' in ssr:
            obfs_param = str( DoBase64( ssr['obfs_param'] ) )
        else:
            obfs_param = str( DoBase64( '' ) )
        protocol_param = str( DoBase64( ssr['protocol_param'] ) )

        params_base64_ipv4 = 'obfsparam=' + obfs_param + '&protoparam=' + protocol_param + '&remarks=' + DoBase64('IPv4') + '&group=' + DoBase64(user)
        params_base64_ipv6 = 'obfsparam=' + obfs_param + '&protoparam=' + protocol_param + '&remarks=' + DoBase64('IPv6') + '&group=' + DoBase64(user)
        ssr_url_ipv4 = ipv4 + ':' + str(port) + ':' + protocol + ':' + method + ':' + obfs  + ':' + passwd + '/?' + params_base64_ipv4
        ssr_url_ipv6 = ipv6 + ':' + str(port) + ':' + protocol + ':' + method + ':' + obfs  + ':' + passwd + '/?' + params_base64_ipv6
#        print ssr_url_ipv4,'\n',ssr_url_ipv6
        ssr_url = 'ssr://' + DoBase64(ssr_url_ipv4) + '\n' + 'ssr://' + DoBase64(ssr_url_ipv6)  + '\n'

        ssr_url_all = ssr_url_all + '\n' + ssr_url

        ssr_url_base64 = DoBase64(ssr_url)
#        print ssr_url
#        print DoBase64( ssr_url )
#        print ' '

        file_r = open('/root/MyVPS/ssr_' + user + '.txt', 'w+')
        file_r.write(ssr_url_base64)
        file_r.write('\n\n\n\n')
        file_r.close()

        file_r = open('/root/MyVPS/' + user + '.ssr', 'w+')
        file_r.write(ssr_url_base64)
        file_r.write('\n\n\n\n')
        file_r.close()

        print '\r',"firewall-cmd --permanent --add-port=" , str(port) , "/tcp"," ==> ",
        sys.stdout.flush()
        os.system("firewall-cmd --permanent --add-port=" + str(port) + "/tcp")
        print '\r',"firewall-cmd --permanent --add-port=" , str(port) , "/udp"," ==> ",
        sys.stdout.flush()
        os.system("firewall-cmd --permanent --add-port=" + str(port) + "/udp")

        os.system("iptables -I INPUT -m state --state NEW -m tcp -p tcp --dport " + str(port) + " -j ACCEPT")
        os.system("iptables -I INPUT -m state --state NEW -m udp -p udp --dport " + str(port) + " -j ACCEPT")
        os.system("ip6tables -I INPUT -m state --state NEW -m tcp -p tcp --dport " + str(port) + " -j ACCEPT")
        os.system("ip6tables -I INPUT -m state --state NEW -m udp -p udp --dport " + str(port) + " -j ACCEPT")
        # end loop
    # end loop
    file_obj.close()


#    file_all = open(os.path.join(mypath, 'All.ssr'), 'w+')
#    file_all.write(DoBase64(ssr_url_all))
#    file_all.write('\n')
#    file_all.close()
else:
    print filename,"does not exists."
############################################################################################################################
### V2RAY ##################################################################################################################
############################################################################################################################

vmess_url_all = ''

os.system("cp /usr/local/etc/v2ray/config.json" + " " + mypath)
filename = "/usr/local/etc/v2ray/config.json"
if os.path.exists(filename):
    file_obj = open(filename,"r")
    # 不带S的loads,针对的是文件的
    v2ray_json = json.load(file_obj)
    # 带S的loads,针对的是字符串的
    temp_json = json.loads\
     (
        '{\
          "v": "2",\
          "ps": "user name",\
          "add": "127.0.0.1",\
          "port": "65535",\
          "id": "00000000-0000-0000-0000-000000000000",\
          "aid": "00",\
          "net": "tcp",\
          "type": "none",\
          "host": "",\
          "path": "",\
          "tls": ""\
        }'
    )

    inbounds = v2ray_json['inbounds']
    for inbound in inbounds:
        # print inbound
        port = inbound['port']
        protocol = inbound['protocol']
        settings = inbound['settings']
        clients = settings['clients']

        # print clients
        # print protocol,ipv4,port

        for client in clients:
            #print client
            id = client['id']
            alterId = client['alterId']
            email = client['email'].split('@')[0]
            # print email,protocol,ipv4,port,id,alterId

            temp_json['v'] = "2"
            temp_json['ps'] = email+'_ipv4'
            temp_json['add'] = ipv4
            temp_json['port'] = port
            temp_json['id'] = id
            temp_json['aid'] = alterId
            temp_json['net'] = "tcp"
            temp_json['type'] = "none"
            temp_json['host'] = ""
            temp_json['path'] = ""
            temp_json['tls'] = ""
            #print temp_json
            temp_vmess_ipv4 = "vmess://"+str(DoBase64(json.dumps(temp_json)))

            temp_json['v'] = "2"
            temp_json['ps'] = email+'_ipv6'
            temp_json['add'] = ipv6
            temp_json['port'] = port
            temp_json['id'] = id
            temp_json['aid'] = alterId
            temp_json['net'] = "tcp"
            temp_json['type'] = "none"
            temp_json['host'] = ""
            temp_json['path'] = ""
            temp_json['tls'] = ""
            #print temp_json
            temp_vmess_ipv6 = "vmess://"+str(DoBase64(json.dumps(temp_json)))

#            print temp_vmess_ipv4,temp_vmess_ipv6
            temp_vmess = temp_vmess_ipv4 + '\n' + temp_vmess_ipv6  + '\n'
            vmess = DoBase64(temp_vmess)

            vmess_url_all = vmess_url_all + '\n' + temp_vmess
#            print vmess

            file_r = open('/root/MyVPS/vmess_' + email + '.txt', 'w+')
            file_r.write(vmess)
            file_r.write('\n\n\n\n')
            file_r.close()

            file_r = open('/root/MyVPS/' + email + '.vmess', 'w+')
            file_r.write(vmess)
            file_r.write('\n\n\n\n')
            file_r.close()

            #end loop
        
        print '\r',"firewall-cmd --permanent --add-port=" , str(port) , "/tcp"," ==> ",
        sys.stdout.flush()
        os.system("firewall-cmd --permanent --add-port=" + str(port) + "/tcp")
        print '\r',"firewall-cmd --permanent --add-port=" , str(port) , "/udp"," ==> ",
        sys.stdout.flush()
        os.system("firewall-cmd --permanent --add-port=" + str(port) + "/udp")

        os.system("iptables -I INPUT -m state --state NEW -m tcp -p tcp --dport " + str(port) + " -j ACCEPT")
        os.system("iptables -I INPUT -m state --state NEW -m udp -p udp --dport " + str(port) + " -j ACCEPT")
        os.system("ip6tables -I INPUT -m state --state NEW -m tcp -p tcp --dport " + str(port) + " -j ACCEPT")
        os.system("ip6tables -I INPUT -m state --state NEW -m udp -p udp --dport " + str(port) + " -j ACCEPT")

        # end loop
    #end loop
    file_obj.close()

#    file_all = open(os.path.join(mypath, 'All.vmess'), 'w+')
#    file_all.write(DoBase64(vmess_url_all))
#    file_all.write('\n')
#    file_all.close()
else:
    print filename,"does not exists."

############################################################################################################################
# apply iptable and firewall settings ######################################################################################
############################################################################################################################
print '\r',"service iptables save"," ==> ",
sys.stdout.flush()
os.system("service iptables save")
os.system("systemctl restart iptables")
print '\r',"service ip6tables save"," ==> ",
sys.stdout.flush()
os.system("service ip6tables save")
os.system("systemctl restart iptables")
print '\r',"firewall-cmd --reload"," ==> ",
sys.stdout.flush()
os.system("systemctl restart firewalld")
os.system("firewall-cmd --reload")
############################################################################################################################
# end of do create all #####################################################################################################
############################################################################################################################

## ---------------------------------------------------------------------------------------------------------------------- ##

############################################################################################################################
# run python DoUpload.py  ######################################################################################
############################################################################################################################
os.system("python2 " + mypath + "/DoUpLoad.py ")
############################################################################################################################
# end of do upload ######################################################################################################### 
############################################################################################################################
